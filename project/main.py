import AI
import model
import Game
import time

ChessList = Game.initChessSquare()
Role = Game.white
resultFlag = 0

def eventHander(x, y, game):            #监听各种事件
    global ChessList, Role, resultFlag
    if resultFlag == 0:
        print("开始计算")
        point = AI.get_chess(ChessList, Role)
        ChessList[point] = Role
        print("结束计算：", point, Role)
        game.move(point[0], point[1], Role)
        if Game.judgeResult(point, Role, ChessList):
            resultFlag = Role
        Role = -Role
def Display():
    model.Load()
    game = Game.Game()
    ChessList = Game.initChessSquare()
    game.start()

    while True:
        game.eventHander(eventHander)
        game.update()

if __name__ == '__main__':
    Display()