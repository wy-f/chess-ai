board_size = 6 #棋盘大小
cell_size = 80 #棋盘格子大小
choose_r = 10 #判断是否下棋的选区半径
zero_point = 27 #左上角第一个棋盘交点坐标
chess_r = 18 #棋子半径
draw_point = False #画点
win_num = 4 #获胜棋子数

white = 1 #白棋
black = -1 #黑棋

weight_path = 'weight.pth'