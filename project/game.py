import pygame        #导入pygame游戏模块
import time
import sys
import os
import numpy as np
from pygame.locals import *
from Setting import *


initChessList = np.zeros((board_size, board_size), np.float32)          #保存棋盘 0:没有棋子 1:白子 2:黑子
initRole = white            #1：代表白棋； 2：代表黑棋
resultFlag = 0              #结果标志



class Point():
    def __init__(self, point):
        '''
        :param x: 代表x轴坐标
        :param y: 代表y轴坐标
        '''
        self.x, self.y = point           #初始化成员变量

    def __add__(self, rhs):
        return Point((self.x + rhs.x, self.y + rhs.y))

    def __sub__(self, rhs):
        return Point((self.x - rhs.x, self.y - rhs.y))



def initChessSquare():     #初始化棋盘
    return np.zeros((board_size, board_size), np.float32) 


def ClickFun(x, y, game):
    global initRole
    if (x + cell_size - zero_point + choose_r) % cell_size <= choose_r * 2 and (y + cell_size - zero_point + choose_r) % cell_size <= choose_r * 2:
        x = int((x - zero_point + choose_r * 2) / cell_size)
        y = int((y - zero_point + choose_r * 2) / cell_size)
        if initChessList[x, y] == 0:
            initChessList[x, y] = initRole
            game.move(x, y, initRole)
            judgeResult((x, y), initRole, initChessList)
            initRole = -initRole

def judgeResult(point, value, ChessList):   #横向判断
    global resultFlag
    flag = False
    for move_point in [Point((0, 1)), Point((1, 0)), Point((1, -1)), Point((1, 1))]:
        count = 1
        point_add = point_sub = Point(point)
        add_flag = sub_flag = True
        while add_flag or sub_flag:
            if add_flag:
                point_add += move_point
                if(point_add.x >= board_size or point_add.y >= board_size or point_add.x < 0 or point_add.y < 0):
                    add_flag = False
                else:
                    if ChessList[point_add.x, point_add.y] == value:
                        count += 1
                    else:
                        add_flag = False
            if sub_flag:
                point_sub -= move_point
                if(point_sub.x >= board_size or point_sub.y >= board_size or point_sub.x < 0 or point_sub.y < 0):
                    sub_flag = False
                else:
                    if ChessList[point_sub.x, point_sub.y] == value:
                        count += 1
                    else:
                        sub_flag = False
            if count >= win_num:
                resultFlag = value #获取成立的棋子颜色
                print("白棋赢" if value == 1 else "黑棋赢")
                return True
    return False

class Game():
    def __init__(self):
        pygame.init()     # 初始化游戏环境
        self.screen = pygame.display.set_mode((620, 620), 0, 0)          # 创建游戏窗口 # 第一个参数是元组：窗口的长和宽
        pygame.display.set_caption("棋")                                # 添加游戏标题
        parent_load = os.path.dirname(os.path.realpath(__file__)) + '/'
        self.background = pygame.image.load(parent_load + "images/bk.jpeg")          #加载背景图片
        for i in range(board_size):                                     #画棋盘线
            pygame.draw.line(self.background, 'black', (zero_point + i * cell_size, zero_point), (zero_point + i * cell_size, zero_point + (board_size - 1) * cell_size))
            pygame.draw.line(self.background, 'black', (zero_point, zero_point + i * cell_size), (zero_point + (board_size - 1) * cell_size, zero_point + i * cell_size))
        if draw_point:                                                  #画原点
            for point in [(3, 3), (3, board_size - 4), (board_size - 4, 3), (board_size - 4, board_size - 4), (int(board_size / 2), int(board_size / 2))]:
                pygame.draw.circle(self.background, 'black', [i * cell_size + zero_point for i in point], 6)
        self.whiteStorn = pygame.image.load(parent_load + "images/storn_white.png")   #加载白棋图片
        self.blackStorn = pygame.image.load(parent_load + "images/storn_black.png")   #加载黑棋图片
        self.resultStorn = pygame.image.load(parent_load + "images/resultStorn.jpg")  #加载赢时的图片
        self.rect = self.blackStorn.get_rect()

    def start(self):
        self.screen.blit(self.background, (0,0))    
        pygame.display.update()

    def move(self, x, y, vlaue):
        if vlaue == white:
            self.screen.blit(self.whiteStorn, (x * cell_size + zero_point - chess_r, y * cell_size + zero_point- chess_r))
        elif vlaue == black:
            self.screen.blit(self.blackStorn,(x * cell_size + zero_point - chess_r, y * cell_size + zero_point - chess_r))
        pygame.display.update()

    def update(self):
        pygame.display.update()

    def win(self):
        self.screen.blit(self.resultStorn, (200, 200)) #绘制获胜时的图片
        pygame.display.update()

    def eventHander(self, click_fun):
        for event in pygame.event.get():
            if event.type == QUIT:#事件类型为退出时
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEBUTTONDOWN: #当点击鼠标时
                x, y = pygame.mouse.get_pos()  #获取点击鼠标的位置坐标
                click_fun(x, y, self)
            


# 主函数
def main():
    global initChessList,resultFlag
    game = Game()
    initChessList = initChessSquare()
    game.start()
    while True:
        if resultFlag != 0:
            initChessList = initChessSquare()             # 重新初始化棋盘
            game.win()
            time.sleep(3)
            resultFlag = 0
            game.start()
        game.eventHander(ClickFun)                         #调用定义的事件函数

if __name__ == '__main__':
    main()        #调用主函数
    pass
